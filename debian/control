Source: knotifications
Section: libs
Priority: optional
Maintainer: Debian/Kubuntu Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Maximiliano Curia <maxy@debian.org>
Build-Depends: cmake (>= 3.5~),
               dbus-x11,
               debhelper-compat (= 13),
               doxygen,
               extra-cmake-modules (>= 5.74.0~),
               graphviz,
               libdbusmenu-qt5-dev,
               libkf5config-dev (>= 5.74.0~),
               libkf5coreaddons-dev (>= 5.74.0~),
               libkf5windowsystem-dev (>= 5.74.0~),
               libphonon4qt5-dev (>= 4.6.60~),
               libphonon4qt5experimental-dev,
               libqt5sql5-sqlite,
               libqt5texttospeech5-dev,
               libqt5x11extras5-dev (>= 5.12.0~),
               pkg-config,
               pkg-kde-tools (>= 0.15.15ubuntu1~),
               qtbase5-dev (>= 5.12.0~),
               qttools5-dev (>= 5.4),
               qttools5-dev-tools (>= 5.4),
               xauth,
               xvfb,
Standards-Version: 4.5.0
Homepage: https://invent.kde.org/frameworks/knotifications
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/knotifications
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/knotifications.git
Rules-Requires-Root: no

Package: libkf5notifications-data
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ${shlibs:Depends}
Breaks: libkf5notifications5 (<< 5.74)
Replaces: libkf5notifications5 (<< 5.74)
Description: Framework for desktop notifications
 KNotification is used to notify the user of an event. It covers
 feedback and persistent events.
 .
 This package contains the translations.

Package: libkf5notifications-dev
Section: libdevel
Architecture: any
Depends: libkf5config-dev (>= 5.74.0~),
         libkf5coreaddons-dev (>= 5.74.0~),
         libkf5notifications5 (= ${binary:Version}),
         libkf5windowsystem-dev (>= 5.74.0~),
         qtbase5-dev (>= 5.12.0~),
         ${misc:Depends},
Breaks: libkf5kdelibs4support-dev (<< 5.54),
        libkf5khtml-dev (<< 5.54),
        libkf5parts-dev (<< 5.54),
Recommends: libkf5notifications-doc (= ${source:Version})
Description: Framework for desktop notifications
 KNotification is used to notify the user of an event. It covers
 feedback and persistent events.
 .
 Contains debug symbols for KNotifications.

Package: libkf5notifications-doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Framework for desktop notifications
 KNotification is used to notify the user of an event. It covers
 feedback and persistent events.
 .
 This package contains the qch documentation files.
Section: doc

Package: libkf5notifications5
Architecture: any
Multi-Arch: same
Depends: libkf5notifications-data (= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Breaks: frameworkintegration (<< 5.54),
        kio (<< 5.54),
        libkf5kdelibs4support5 (<< 5.54),
        libkf5kdelibs4support5-bin (<< 5.42),
        libkf5khtml5 (<< 5.54),
        libkf5plasma5 (<< 5.54),
        libkf5wallet-bin (<< 5.54),
        libkwalletbackend5-5 (<< 5.54),
        plasma-framework (<< 5.54),
Description: Framework for desktop notifications
 KNotification is used to notify the user of an event. It covers
 feedback and persistent events.
